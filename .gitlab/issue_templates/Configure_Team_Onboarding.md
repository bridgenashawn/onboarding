## Welcome to GitLab and the Configure Team!

We are all excited that you are joining us on the Configure Team.  You should have already received an onboarding issue from People Ops familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Configure Team people, processes and setup.
For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Configure team.
Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day
* [ ] Manager: Invite team member to #s_configure Slack Channel
* [ ] Manager: Invite team member to Weekly Team Meetings
* [ ] Manager: Add new team member to Configure Team Retro



### First Week
* [ ] Manager: Add new team member to [geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Introduce new team members in relevant Slack channels
* [ ] New team member: Read about your team, its mission, team members and resources on the [Configure Team page](https://about.gitlab.com/handbook/engineering/development/ops/configure/)
* [ ] New team member: Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet your team

### Second Week
* [ ] New team member: Read about how Gitlab uses labels [Issue Workflow Labels](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md), [Labels FOSS](https://gitlab.com/gitlab-org/gitlab-foss/-/labels)
* [ ] New team member: Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
* [ ] New team member: Familiarize yourself with the team boards [Configure Team Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1223426)
* [ ] New team member: Please review this onboarding issue and update the template with some improvements as you see fit to make it easier for the next newbie!

### Third Week
* [ ] New team member: Setup and run AutoDevops
* [ ] New team member: Setup and run a Cluster on GKE
* [ ] New team member: Setup and run a Cluster on EKS
* [ ] New team member: Familiarize yourself with our [Terraform integrations](https://docs.gitlab.com/ee/user/infrastructure/)
* [ ] New team member: Familiarize yourself with our [Kubernetes Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent)
* [ ] New team member: Familiarize yourself with the team repos outlined below

## Processes
#### Engineering Workflow

* [https://about.gitlab.com/handbook/engineering/workflow/](https://about.gitlab.com/handbook/engineering/workflow/)

#### Important Dates

* [https://about.gitlab.com/handbook/product/#important-dates-pms-should-keep-in-mind](https://about.gitlab.com/handbook/product/#important-dates-pms-should-keep-in-mind)
* [https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

#### Workflow Labels

* https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#workflow-labels

#### Merge Request Workflow

* https://docs.gitlab.com/ee/development/code_review.html

#### Developing with Feature Flags
Consider using feature flags for every medium size-feature:

* https://docs.gitlab.com/ee/development/rolling_out_changes_using_feature_flags.html
* https://docs.gitlab.com/ee/development/feature_flags.html

#### Testing Best Practices

* https://docs.gitlab.com/ee/development/testing_guide/best_practices.html

### Team Meeting Youtube Playlist
* [Youtube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kr4QHduFqdnVSEFeBy_rNeO)

### Learning Materials
* [Product vision](https://about.gitlab.com/direction/configure/)
* [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/)
* [Adding and removing Kubernetes Clusters](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html)
* [Configuring Kubernetes Clusters](https://docs.gitlab.com/ee/user/project/clusters/#cluster-configuration)

## Setting up your local environment / Getting started tasks

To use some of Configure’s product categories in your local environment, like Auto DevOps and Kubernetes Management, you have to set up some extra features in your GDK installation. These guides will help you to get up and running:

#### Using GKE integration

First, you’ll have to request access to the GitLab GCP project if you haven’t already. Create an issue in the access-request project using the [`Single Person Access Request`](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue template. The following issue can be used as a reference: https://gitlab.com/gitlab-com/access-requests/issues/1664. Afterward, follow the [Enable Google OAuth2 guide](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/google-oauth2.md) to set up your GDK instance.

#### Using EKS integration

First, you'll need to request access to the GitLab AWS project if you haven't already. Create an issue in the access-request project using the [`Single Person Access Request`](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue template. The following issue can be used as a reference: https://gitlab.com/gitlab-com/access-requests/issues/3425. Afterward, configure your local instance using the same [documentation written for self-managed instances]( https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#additional-requirements-for-self-managed-instances-core-only) and then follow the [Add New EKS cluster docs](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#new-eks-cluster) to create your cluster.

#### Running Auto DevOps

As with GKE, you’ll need to request special access to run Auto DevOps. Create an issue in the access-request project using the [`Single Person Access Request`](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue template. The following issue can be used as a reference: https://gitlab.com/gitlab-com/access-requests/issues/944. Afterward, follow the [GDK Auto DevOps guide](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops.md) to set up your GDK instance. It is also useful to run an instance of the [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/runner.md) in your local environment.

### Team Repos

There are several projects that the Configure team is primarily responsible for. Feel free to familiarize yourself with the project below:

* [helm-install-image](https://gitlab.com/gitlab-org/cluster-integration/helm-install-image/)
* [auto-deploy-app](https://gitlab.com/gitlab-org/charts/auto-deploy-app)
* [auto-build-image](https://gitlab.com/gitlab-org/cluster-integration/auto-build-image)
* [auto-deploy-image](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image)
* [cluster-applications](https://gitlab.com/gitlab-org/cluster-integration/cluster-applications/issues/3)
* [terraform-images](https://gitlab.com/gitlab-org/terraform-images)
* [cluster-management-project](https://gitlab.com/gitlab-org/project-templates/cluster-management/)
* [gitlab-agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/)
* [terraform-provider-gitlab](https://github.com/gitlabhq/terraform-provider-gitlab)
/confidential
/due in 21 days
